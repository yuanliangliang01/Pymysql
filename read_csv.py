import  csv
class CSVUtil:
    def __init__(self,file_path):
        self.file_path=file_path
    def get_list_data(self):
        values=[]
        f=open(self.file_path,'r',encoding='utf-8')   # 'r'  表示读模式
        c=csv.reader(f)
        next(c)
        for row in c:
            values.append(row)
        f.close()
        return values
if __name__ == '__main__':
    u=CSVUtil('..\\testdata\\data1.csv')
    d=u.get_list_data()
    print(d)